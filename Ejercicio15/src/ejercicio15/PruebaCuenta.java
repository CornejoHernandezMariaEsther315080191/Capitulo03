/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio15;

import static ejercicio15.Ejercicio15.mostrarCuenta;
import java.util.Scanner;

/**
 *
 * @author Cornejo Hernández María Esther
 */
public class PruebaCuenta {
    public static void main(String[] args){
     Ejercicio15 cuenta1 = new Ejercicio15("Jane Green", 50.00);
     Ejercicio15 cuenta2 = new Ejercicio15("John Blue", -7.53);
     Scanner entrada = new Scanner(System.in);
    // muestra el saldo inicial de cada objeto
   /*
    System.out.printf("Saldo de %s: $%.2f%n",
            cuenta1.obtenerNombre(), cuenta1.obtenerSaldo());
    System.out.printf("Saldo de %s: $%.2f%n%n",
            cuenta2.obtenerNombre(), cuenta2.obtenerSaldo()); 
   */
    mostrarCuenta(cuenta1); 
    mostrarCuenta(cuenta2); 

    System.out.print("Escriba el monto a depositar para cuenta1: ");
    double montoDeposito = entrada.nextDouble(); // obtiene entrada del usuario
    System.out.printf("%nsumando %.2f al saldo de cuenta1%n%n",
    montoDeposito);
    cuenta1.depositar(montoDeposito); // suma al saldo de cuenta1

    // muestra los saldos
    /*System.out.printf("Saldo de %s: $%.2f%n",
    cuenta1.obtenerNombre(), cuenta1.obtenerSaldo()); 
    System.out.printf("Saldo de %s: $%.2f%n%n",
    cuenta2.obtenerNombre(), cuenta2.obtenerSaldo());*/
    
    mostrarCuenta(cuenta1); 
    mostrarCuenta(cuenta2); 
    System.out.print("Escriba el monto a depositar para cuenta2: ");
    montoDeposito = entrada.nextDouble(); // obtiene entrada del usuario
    System.out.printf("%nsumando %.2f al saldo de cuenta2%n%n",
    montoDeposito);
    cuenta2.depositar(montoDeposito); // suma al saldo de cuenta2

    // muestra los saldos
   /*
    System.out.printf("Saldo de %s: $%.2f%n",cuenta1.obtenerNombre(), cuenta1.obtenerSaldo());
    System.out.printf("Saldo de %s: $%.2f%n%n",cuenta2.obtenerNombre(), cuenta2.obtenerSaldo()); 
   */
    mostrarCuenta(cuenta1); 
    mostrarCuenta(cuenta2); 

    System.out.print("Escriba el monto a retirar para cuenta1: "); 
    double montoRetiro = entrada.nextDouble();// obtiene entrada del usuario
    System.out.printf("%nRetirando %.2f al saldo de cuenta1%n%n",
    montoRetiro);
    cuenta1.retirar(montoRetiro); //resta al saldo de cuenta1

    // muestra los saldos
    /*
    System.out.printf("Saldo de %s: $%.2f%n",
    cuenta1.obtenerNombre(), cuenta1.obtenerSaldo()); 
    System.out.printf("Saldo de %s: $%.2f%n%n",
    cuenta2.obtenerNombre(), cuenta2.obtenerSaldo()); 
   */
     mostrarCuenta(cuenta1); 
    mostrarCuenta(cuenta2); 
    System.out.print("Escriba el monto a retirar para cuenta2: "); 
    montoRetiro = entrada.nextDouble(); // obtiene entrada del usuario
    System.out.printf("%nRetirando %.2f al saldo de cuenta2%n%n",
    montoRetiro);
    cuenta2.retirar(montoRetiro); // resta al saldo de cuenta2

    // muestra los saldos
    /*
   System.out.printf("Saldo de %s: $%.2f%n",cuenta1.obtenerNombre(), cuenta1.obtenerSaldo());
    System.out.printf("Saldo de %s: $%.2f%n%n",cuenta2.obtenerNombre(), cuenta2.obtenerSaldo());
     */
     mostrarCuenta(cuenta1); 
     mostrarCuenta(cuenta2); 
    // TODO code application logic here
       }
}
