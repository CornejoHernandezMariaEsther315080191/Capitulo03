/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio17;

import java.util.Scanner;

/**
 *
 * @author Cornejo Hernández María Esther
 */
public class PruebaPerfilMedico {
    public static void main(String[] args){
        
        String Nombre, Apellido, sexo;
        short Dia, Mes;
        int Año;
        double altura, peso;
        
        Scanner s=new Scanner(System.in);
        System.out.print("Nombre: ");
        Nombre=s.nextLine();
        System.out.print("Apellido Paterno: ");
        Apellido=s.nextLine();
        System.out.print("Día de nacimiento: ");
        Dia=s.nextShort();
        System.out.print("Mes de nacimiento: ");
        Mes=s.nextShort();
        System.out.print("Año de nacimiento: ");
        Año=s.nextInt();
        System.out.print("Sexo: ");
        sexo=s.next();
        System.out.print("Altura (cm): ");
        altura=s.nextDouble();
        System.out.print("Peso(kg): ");
        peso=s.nextDouble();
        
        Ejercicio17 Paciente=new Ejercicio17(Nombre, Apellido, 
                sexo, Dia, Mes, Año, altura, peso);
        Paciente.Paciente();
        Paciente.EdadPaciente();
        Paciente.CalculoIMC();
        Paciente.FrecuenciaCarciadaMax();
        Paciente.FrecuenciaCardiacaESp();
    }
    
}
