/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio16;

/**
 * 3.16 (Calculadora de la frecuencia cardiaca esperada) Mientras se ejercita, puede usar un monitor de frecuencia
cardiaca para ver que su corazón permanezca dentro de un rango seguro sugerido por sus entrenadores y doctores.
De acuerdo con la Asociación Estadounidense del Corazón (AHA) (www.americanheart.org/presenter.
jhtml?identifier=4736), la fórmula para calcular su frecuencia cardiaca máxima en pulsos por minuto es 220
menos su edad en años. Su frecuencia cardiaca esperada tiene un rango que está entre el 50 y el 85% de su frecuencia
cardiaca máxima. [Nota: estas fórmulas son estimaciones proporcionadas por la AHA. Las frecuencias cardiacas
máxima y esperada pueden variar con base en la salud, condición física y sexo del individuo. Siempre debe consultar
un médico o a un profesional de la salud antes de empezar o modificar un programa de ejercicios]. Cree una
clase llamada FrecuenciasCardiacas. Los atributos de la clase deben incluir el primer nombre de la persona, su apellido
y fecha de nacimiento (la cual debe consistir de atributos independientes para el mes, día y año de nacimiento).
Su clase debe tener un constructor que reciba estos datos como parámetros. Para cada atributo debe proveer métodos
establecer y obtener. La clase también debe incluir un método que calcule y devuelva la edad de la persona (en años),
un método que calcule y devuelva la frecuencia cardiaca máxima de esa persona, y otro método que calcule y devuelva
la frecuencia cardiaca esperada de la persona. Escriba una aplicación de Java que pida la información de la persona,
cree una instancia de un objeto de la clase FrecuenciasCardiacas e imprima la información a partir de ese objeto
(incluyendo el primer nombre de la persona, su apellido y fecha de nacimiento), y que después calcule e imprima la
edad de la persona en (años), frecuencia cardiaca máxima y rango de frecuencia cardiaca esperada.
 */


/**
 *
 * @author Cornejo Hernández María Esther
 */
public class Ejercicio16 {
    
    String Nombre;
    String Apellido;
    short día;
    short mes;
    int año;
    
    public Ejercicio16(String Nombre, String Apellido, short día, short mes, int año){
        this.Nombre=Nombre;
        this.Apellido=Apellido;
        this.año=año;
        
        if(día > 0)
        this.día=día;
        
        if(mes > 0)
        this.mes=mes;
        
        
    }
    
    
    public void establecerNombre(String Nombre){
        this.Nombre = Nombre;
    }

    public String obtenerNombre(){
        return Nombre; 
    }
    
    public void establecerApellido(String Apellido){
        this.Apellido = Apellido;
    }

    public String obtenerApellido(){
        return Apellido; 
    }
    
    public void establecerDia(short día){
        this.día = día;
    }

    public short obtenerDia(){
        return día; 
    }
    
    public void establecerMes(short mes){
        this.mes=mes;
    }

    public short obtenerMes(){
        return mes; 
    }
    
    public void establecerAño(int año){
        this.año= año;
    }

    public int obtenerAño(){
        return año; 
    }
    
    public void Paciente(){
        
        System.out.printf("%nFecha en que ingresó el Paciente: 31 Diciembre 2018%n"
                + "%nPaciente: %s %s%nFecha de nacimiento: %s/%s/%s",
                obtenerApellido(), obtenerNombre(), obtenerDia(), obtenerMes(),
                obtenerAño());
        
        if(obtenerDia() < 0 || obtenerDia() > 31)
        System.out.print("Error con la fecha de nacimiento.");
        if(obtenerMes() < 0 || obtenerMes() > 12)
        System.out.print("Errror con la fecha de nacimiento.");
        if(obtenerAño() > 2018)
        System.out.print("Error con la fecha de nacimiento.");
        
    }
    
    public void EdadPaciente(){
        
        int EdadPaciente=2018-obtenerAño();
        
        System.out.printf("%nLa edad del paciente %s es: %s%n", obtenerApellido(),
                EdadPaciente);
        
        if(obtenerDia() < 0 || obtenerDia() > 31)
        System.out.print("Error con la fecha de nacimiento.");
        if(obtenerMes() < 0 || obtenerMes() > 12)
        System.out.print("Errror con la fecha de nacimiento.");
        if(obtenerAño() > 2018)
        System.out.print("Error con la fecha de nacimiento.");
        
    }
    
    public void FrecuenciaCarciadaMax(){
        int EdadPaciente=obtenerAño()-2018;
        int FrecuenciaCardiacaMax=220-EdadPaciente;
        
        System.out.printf("%nLa frecuencia cardiaca máxima del paciente en pulsos por minuto"
                + "es de: %s%n (De acuerdo con la Asociación Estadounidense del Corazón (AHA))%n",
                FrecuenciaCardiacaMax);
        if(obtenerDia() < 0 || obtenerDia() > 31)
        System.out.print("Error con la fecha de nacimiento.");
        if(obtenerMes() < 0 || obtenerMes() > 12)
        System.out.print("Errror con la fecha de nacimiento.");
        if(obtenerAño() > 2018)
        System.out.print("Error con la fecha de nacimiento.");
    }
    
    
    
    public void FrecuenciaCardiacaESp(){
        int EdadPaciente=obtenerAño()-2018;
        int FrecuenciaCardiacaMax=220-EdadPaciente;
        double FrecuenciaCardiacaEsp=67.5*FrecuenciaCardiacaMax/100;
        
        System.out.printf("%nLa frecuencia cardiaca esperada del paciente %s en pulsos por minuto"
                + "es de: %.2f%n "
                + "(La frecuencia cardiaca esperada se encuentra entre el 50 y 85 "
                + "porciento de la frecuencia cardiaca máxima, "
                + "por lo que tomaremos el 65.7 porciento. "
                + "De acuerdo con la Asociación Estadounidense del Corazón (AHA))%n",
                obtenerApellido(),FrecuenciaCardiacaEsp);
        
        if(obtenerDia() < 0 || obtenerDia() > 31)
        System.out.print("Error con la fecha de nacimiento.");
        if(obtenerMes() < 0 || obtenerMes() > 12)
        System.out.print("Errror con la fecha de nacimiento.");
        if(obtenerAño() > 2018)
        System.out.print("Error con la fecha de nacimiento.");
    }
   
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}
