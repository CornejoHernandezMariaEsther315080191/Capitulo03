/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio16;
import java.util.Scanner;

/**
 *
 * @author Cornejo Hernández María Esther
 */
public class PruebaFrecuenciasCardiacas {
    
    public static void main(String[] args){
        String Nombre, Apellido;
        short Dia, Mes;
        int Año;
        
        Scanner s=new Scanner(System.in);
        System.out.print("Nombre: ");
        Nombre=s.nextLine();
        System.out.print("Apellido Paterno: ");
        Apellido=s.nextLine();
        System.out.print("Día de nacimiento: ");
        Dia=s.nextShort();
        System.out.print("Mes de nacimiento: ");
        Mes=s.nextShort();
        System.out.print("Año de nacimiento: ");
        Año=s.nextInt();
        
        Ejercicio16 Paciente=new Ejercicio16(Nombre, Apellido, Dia, Mes, Año);
        Paciente.Paciente();
        Paciente.EdadPaciente();
        Paciente.FrecuenciaCarciadaMax();
        Paciente.FrecuenciaCardiacaESp();
    }
    
}
