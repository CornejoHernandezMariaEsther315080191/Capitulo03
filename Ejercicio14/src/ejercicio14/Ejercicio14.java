/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio14;
/**
 3.14 (La clase Fecha) Cree una clase llamada Fecha, que incluya tres variables de instancia: un mes (tipo int), 
 un día (tipo int) y un año (tipo int). Su clase debe tener un constructor que inicialice las tres variables de 
 instancia, y debe asumir que los valores que se proporcionan son correctos. Proporcione un método establecer y 
 un método obtener para cada variable de instancia. Proporcione un método mostrar Fecha, que muestre el mes, 
 día y año, separados por barras diagonales (/). Escriba una aplicación de prueba llamada PruebaFecha, que demuestre 
 las capacidades de la claseFecha. 
 */

/**
 *
 * @author Cornejo Hernández María Esther
 */
public class Ejercicio14 {
    int mes;
    int dia;
    int año;
    
    public Ejercicio14(int mes, int dia, int año){
    this.mes=mes;
    this.dia = dia;
    this.año= dia;
    }
    
    public void establecerMes(int mes) {
        this.mes = mes;
    }

    public int obtenerMes() {
        return mes;
    }
    
    public void establecerDia(int dia) {
        this.dia = dia;
    }
    
    public int obtenerDia() {
        return dia;
    }

    public void establecerAño(int año) {
        this.año = año;
    }
    
    public int obtenerAño() {
        return año;
    }

    
    
   @Override public String toString(){
        String fecha = obtenerMes() + " / " + obtenerDia() + " / "
            + obtenerAño();
        return fecha;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}
