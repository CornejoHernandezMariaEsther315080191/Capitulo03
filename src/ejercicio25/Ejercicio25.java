/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio25;
import java.util.Scanner;


/**
 *
 * @author Cornejo Hernández María Esther
 */
public class Ejercicio25 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /**
         * (Par o impar) Escriba una aplicación que lea un entero y 
         * que determine e imprima si es impar o par [sugerencia: use el 
         * operador residuo. Un número par es un múltiplo de 2. 
         * Cualquier múltiplo de 2 deja un residuo de 0 cuando se divide 
         * entre 2]. 
         */
        Scanner s=new Scanner(System.in);
        int n1;
        
        System.out.println("Escriba un entero: ");
        n1=s.nextInt();
        
        if(n1%2 == 0)
            System.out.printf("%d es par%n", n1);
        if (n1%2 !=0)
            System.out.printf("%d es impar%n", n1);

    }
    
}
