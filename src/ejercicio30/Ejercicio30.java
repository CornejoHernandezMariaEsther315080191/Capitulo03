/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio30;
import java.util.Scanner;

/**
 *
 * @author Cornejo Hernández María Esther
 */
public class Ejercicio30 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /**
         * (Separación de los dígitos en un entero) Escriba una aplicación que reciba del usuario un número compuesto por cinco dígitos, que separe ese número en sus dígitos individuales y los imprima, cada uno separado de los demás por tres espacios. Por ejemplo, si el usuario escribe el número 42339, Suponga que el usuario escribe el número correcto de dígitos. 
         */
         Scanner s=new Scanner(System.in);
                  int n;
        
                  System.out.print("Dame un número compuesto por 5 dígitos\n");
                  n=s.nextInt();
        
        
                 System.out.printf("%d   ",(int)n/10000);
                     System.out.printf("%d   ", (int)((n%10000)/1000));
                     System.out.printf("%d   ", (int)((n%10000)%1000)/100);
                     System.out.printf("%d   ", (int)(((n%10000)%1000)%100)/10);
                     System.out.printf("%d%n", (int)(((n%10000)%1000)%100)%10);

    }
    
}
