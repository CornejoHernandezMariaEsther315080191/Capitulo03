/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio26;
import java.util.Scanner;

/**
 *
 * @author Cornejo Hernández María Esther
 */
public class Ejercicio26 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /**
         * (Múltiplos) Escriba una aplicación que lea dos enteros, 
         * determine si el primero es un múltiplo del segundo e imprima el 
         * resultado. [Sugerencia: use el operador residuo].
         */
        Scanner s=new Scanner(System.in);
        int n1, n2;
        
        System.out.println("Escriba un entero: ");
        n1=s.nextInt();
        System.out.println("Escriba un entero: ");
        n2=s.nextInt();

        if(n1%n2==0)
            System.out.printf("%d es múltiplo de %d%n", n1, n2);
        if(n1%n2 != 0)
            System.out.printf("%d y %d no son múltiplos%n", n1, n2);

    }
    
}
