/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio28;

import java.util.Scanner;

/**
 *
 * @author Cornejo Hernández María Esther
 */
public class Ejercicio28 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /**
         * (Diámetro, circunferencia y área de un círculo) He aquí un adelanto. 
         * En este capítulo aprendió sobre los enteros y el tipo int. Java 
         * también puede representar números de punto flotante que contienen 
         * puntos decimales, como 3.14159. Escriba una aplicación que reciba 
         * del usuario el radio de un círculo como un entero, y que imprima el 
         * diámetro, la circunferencia y el área del círculo mediante el uso 
         * del valor de punto flotante 3.14159 para π. Use las técnicas que se 
         * muestran en la figura 2.7 [nota: también puede utilizar la constante 
         * predefinida Math.PI para el valor de π. Esta constante es más precisa 
         * que el valor 3.14159. La clase Math se define en el paquete java.lang. 
         * Las clases en este paquete se importan de manera automática, por lo 
         * que no necesita importar la clase Math mediante la instrucción import 
         * para usarla]. Use las siguientes fórmulas (r es el radio):
         * diámetro = 2r
         * circunferencia = 2πr
         * No almacene los resultados de cada cálculo en una variable. En vez de ello, 
         * especifique cada cálculo como el valor que se imprimirá en una instrucción 
         * System.out.printf. Los valores producidos por los cálculos del área y de la 
         * circunferencia son números de punto flotante. Dichos valores pueden imprimirse 
         * con el especificador de formato %f en una instrucción System.out.printf. En el 
         * capítulo 3 aprenderá más acerca de los números de punto flotante.
         */
        Scanner s=new Scanner(System.in);
        int r;
        
        System.out.print("Dé el radio de la circunferencia: ");
        r=s.nextInt();
        
        System.out.printf("%f es el diámetro%n", 2*r);
        System.out.printf("%f es la circunferencia del círculo%n",
                     2*(Math.PI)*r);
        System.out.printf("%f es el área del circulo%n", (Math.PI)*2*r);

    }
    
}
