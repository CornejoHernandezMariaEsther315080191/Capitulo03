/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio24;
import java.util.Scanner;

/**
 *
 * @author Cornejo Hernández María Esther
 */
public class Ejercicio24 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /**
         * (Enteros menor y mayor) Escriba una aplicación que lea cinco 
         * enteros y que determine e imprima los enteros mayor y menor en el 
         * grupo. Use solamente las técnicas de programación que aprendió en 
         * este capítulo.
         */
        Scanner s=new Scanner(System.in);
        int n1, n2, n3, n4, n5;
        System.out.println("Escriba cinco números enteros distintos entre sí: ");
        n1=s.nextInt();
        n2=s.nextInt();
        n3=s.nextInt();
        n4=s.nextInt();
        n5=s.nextInt();
           
        if(n1>n2 & n1>n3 & n1>n4 & n1> n5)
            System.out.printf("El primero: %d es el mayor de los cinco%n", n1);
        if(n2>n1 & n2>n3 & n2>n4 & n2> n5)
            System.out.printf("El segundo: %d es el mayor de los cinco%n", n2);
        if(n3>n1 & n3>n2 & n3>n4 & n3> n5)
            System.out.printf("El tercero: %d es el mayor de los cinco%n", n3);
        if(n4>n1 & n4>n2 & n4>n3 & n4> n5)
            System.out.printf("El cuarto: %d es el mayor de los cinco%n", n4);
        if(n5>n1 & n5>n2 & n5>n3 & n5> n4)
            System.out.printf("El quinto: %d es el mayor de los cinco%n", n5);

        if(n1<n2 & n1<n3 & n1<n4 & n1< n5)
            System.out.printf("El primero: %d es el menor de los cinco%n", n1);
        if(n2<n1 & n2<n3 & n2<n4 & n2<n5)
            System.out.printf("El segundo: %d es el menor de los cinco%n", n2);
        if(n3<n1 & n3<n2 & n3<n4 & n3< n5)
            System.out.printf("El tercero: %d es el menor de los cinco%n", n3);
        if(n4<n1 & n4<n2 & n4<n3 & n4<n5)
            System.out.printf("El cuarto: %d es el menor de los cinco%n", n4);
        if(n5<n1 & n5<n2 & n5<n3 & n5<n4)
            System.out.printf("El quinto: %d es el mayor de los cinco%n", n5);

    }
    
}
